var appDemo = angular.module("appDemo",[]);  //Se instancia el nombre del modulo appDemo

    appDemo.controller("controlador",function ($scope) { //controlador controlador para la aplicacion appDemo
    var lista = this; //objeto lista igual a this(scope).Se puede poner this que es igual al scope
    lista.amigos = [  //array de objetos JS amigos
        { id: [1], nombre:"JHON", descripcion:"who is 25 years old"},
        { id: [2], nombre:"JESSIE", descripcion:"who is 30 years old"},
        { id: [3], nombre:"JOHANNA", descripcion:"who is 28 years old"},
        { id: [4], nombre:"JOY", descripcion:"who is 15 years old"},
        { id: [5], nombre:"MARY", descripcion:"who is 28 years old"},
        { id: [6], nombre:"PETER", descripcion:"who is 95 years old"},
        { id: [7], nombre:"SEBASTIAN", descripcion:"who is 50 years old"},
        { id: [8], nombre:"ERIKA", descripcion:"who is 27 years old"},
        { id: [9], nombre:"PATRICK", descripcion:"who is 40 years old"},
        { id: [10], nombre:"SAMANTHA", descripcion:"who is 60 years old"}
    ];

    // se puede crear mas arreglos a partir de la var creada. Ejemplo: lista.productos[]
    // Tambien se puede crear mas controladores y modelos
                 
    
});
          
                
